#include "Border.h"



Border::Border()
{
}


Border::~Border()
{
}

Border::Border(exVector2 inp1, exVector2 inp2, exColor col)
{
	p1 = inp1;
	p2 = inp2;
	c = col;
}

void Border::Render(exEngineInterface * mEngine)
{
	exEngineInterface* pEngine = mEngine;
	mEngine->DrawLineBox(p1, p2, c, 1);
}

void Border::Tick(float tDelta)
{
}

