#pragma once
#include "GameObject.h"
class Gliders :public GameObject
{
public:
	Gliders();
	~Gliders();
	Gliders(exVector2 p1,exVector2 p2, exColor c);
	void Render(exEngineInterface* mEngine) override;
	void Tick(float tDelta) override;
};

