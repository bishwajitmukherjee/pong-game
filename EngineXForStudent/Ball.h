#pragma once
#include "GameObject.h"
class Ball :public GameObject
{
public:
	Ball();
	Ball(exVector2 p1, exVector2 p2, exColor c);
	~Ball();
	void Render(exEngineInterface* mEngine) override;
	void Tick(float tDelta) override;
	void Colission(exVector2 pointA, exVector2 pointB) override;
	
};


