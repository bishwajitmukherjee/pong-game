#include "Gliders.h"



Gliders::Gliders()
{
}


Gliders::~Gliders()
{

}

Gliders::Gliders(exVector2 inp1,exVector2 inp2, exColor col)
{
	p1 = inp1;
	p2 = inp2;
	c = col;
}

void Gliders::Render(exEngineInterface * mEngine)
{
	exEngineInterface* pEngine = mEngine;
	mEngine->DrawBox(p1, p2, c, 1);
}

void Gliders::Tick(float tDelta)
{
}
