//
// * ENGINE-X
// * SAMPLE GAME
//
// + Game.h
// definition of MyGame, an implementation of exGameInterface
//

#include "Game/Public/GameInterface.h"
#include "Engine/Public/EngineTypes.h"
#include "Ball.h"
#include "Gliders.h"
#include "Border.h"
//#include "GameObject.h"

//-----------------------------------------------------------------
//-----------------------------------------------------------------

class MyGame : public exGameInterface
{
public:

								MyGame();
	virtual						~MyGame();

	virtual void				Initialize( exEngineInterface* pEngine );

	virtual const char*			GetWindowName() const;
	virtual void				GetClearColor( exColor& color ) const;

	virtual void				OnEvent( SDL_Event* pEvent );
	virtual void				OnEventsConsumed();

	virtual void				Run( float fDeltaT );


private:

	exEngineInterface*			mEngine;

	int							mFontID;
	int							Player1Score;
	int							Player2Score;

	bool						mUp;
	bool						mDown;
	bool						mW;
	bool						mS;


	exVector2					mTextPosition;

	Ball*						myBall;
	Gliders*					myLeftGlider;
	Gliders*					myRightGlider;
	Gliders*					myTopBoundary;
	Gliders*					myBottomBoundary;
	Border*						myBorder;

};
