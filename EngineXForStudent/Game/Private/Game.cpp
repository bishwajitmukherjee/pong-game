//
// * ENGINE-X
// * SAMPLE GAME
//
// + Game.cpp
// implementation of MyGame, an implementation of exGameInterface
//

#include "Game/Private/Game.h"
#include "Ball.h"
#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"
#include"MathLib.h"
#include <stdlib.h>
#include <time.h>
#include<string>
#include <iostream>  
//-----------------------------------------------------------------
//-----------------------------------------------------------------

const char* gWindowName = "Pong";

//-----------------------------------------------------------------
//-----------------------------------------------------------------

MyGame::MyGame()
	: mEngine( nullptr )
	, mFontID( -1 )
	, mUp( false )
	, mDown( false )
	,mW(false)
	, mS(false)
{
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

MyGame::~MyGame()
{
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::Initialize( exEngineInterface* pEngine )
{
	mEngine = pEngine;

	mFontID = mEngine->LoadFont("Evil Empire.otf", 32);

	//mFontID = mEngine->LoadFont("PressStart2P-Regular.ttf", 24);
	exColor col = {255,25,147,255};
	//set ball
	myBall = new Ball(exVector2{ 400.f,100.f }, exVector2{ 5,2 },col);

	//set left and right glider
	myLeftGlider = new Gliders(exVector2{ 0,100 }, exVector2{ 20,250 }, exColor{212,175,55,255});;
	myRightGlider = new Gliders(exVector2{ 800,100 }, exVector2{ 780,250 }, exColor{ 212,175,200,255 });

	//set top and bottom boundary
	myTopBoundary = new Gliders(exVector2{ 0,0 }, exVector2{ 800,20 }, exColor{ 0,130,127,255 });;
	myBottomBoundary = new Gliders(exVector2{ 0,600 }, exVector2{ 800,580 }, exColor{ 0,130,127,255 });

	Player1Score = 0;
	Player2Score = 0;
	
	mTextPosition.x = 50.0f;
	mTextPosition.y = 50.0f;
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

const char* MyGame::GetWindowName() const
{
	return gWindowName;
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::GetClearColor( exColor& color) const
{
	color.mColor[0] = 0;
	color.mColor[1] = 0;
	color.mColor[2] = 0;
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::OnEvent( SDL_Event* pEvent )
{
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::OnEventsConsumed()
{
	int nKeys = 0;
	const Uint8 *pState = SDL_GetKeyboardState( &nKeys );

	mUp = pState[SDL_SCANCODE_UP];
	mDown = pState[SDL_SCANCODE_DOWN];
	mW = pState[SDL_SCANCODE_W];
	mS = pState[SDL_SCANCODE_S];
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::Run( float fDeltaT )
{

	exVector2 p1 = {0.0f,0.0f}, p2 = { 0.0f,0.0f };
	exColor c;
	float r;

	//player1
	if (mUp)
	{
		myRightGlider->p1.y = clamp(myRightGlider->p1.y -= 200.0f * fDeltaT, 20.0f, 430.0f);		
		myRightGlider->p2.y = clamp(myRightGlider->p2.y -= 200.0f * fDeltaT, 170.0f, 580.0f);

	}
	else if (mDown)
	{
		myRightGlider->p1.y = clamp(myRightGlider->p1.y += 200.0f * fDeltaT, 20.0f, 430.0f);
		myRightGlider->p2.y = clamp(myRightGlider->p2.y += 200.0f * fDeltaT, 170.0f, 580.0f);
	}
	//player2
	if (mW)
	{
		myLeftGlider->p1.y = clamp(myLeftGlider->p1.y -= 200.0f * fDeltaT, 20.0f,430.0f);
		myLeftGlider->p2.y = clamp(myLeftGlider->p2.y -= 200.0f * fDeltaT, 170.0f, 580.0f);

	}
	else if (mS)
	{
		myLeftGlider->p1.y = clamp(myLeftGlider->p1.y += 200.0f * fDeltaT, 20.0f, 430.0f);
		myLeftGlider->p2.y = clamp(myLeftGlider->p2.y += 200.0f * fDeltaT, 170.0f, 580.0f);
	}

	//rendering boundaries
	myTopBoundary->Render(mEngine);
	myBottomBoundary->Render(mEngine);
	//rendering the ball and gliders
	myBall->Render(mEngine);
	myLeftGlider->Render(mEngine);
	myRightGlider->Render(mEngine);

	myBall->Tick(fDeltaT);
	//checking colliding with the boundaries and the gliders
	myBall->Colission(myLeftGlider->p1,myLeftGlider->p2);
	myBall->Colission(myRightGlider->p1, myRightGlider->p2);
	myBall->Colission(myTopBoundary->p1, myTopBoundary->p2);
	myBall->Colission(myBottomBoundary->p1, myBottomBoundary->p2);


	//ScoreText
	std::string str1 = std::to_string(Player1Score);
	const char *c1 = str1.c_str();

	std::string str2 = std::to_string(Player2Score);
	const char *c2 = str2.c_str();
	mEngine->DrawText(mFontID, exVector2{ 325,100 }, "Just Pong", exColor{ 225,225,225,255 }, 1);
	mEngine->DrawText(mFontID, exVector2{ 100,300 }, c1, exColor{ 212,175,55,255 }, 1);
	mEngine->DrawText(mFontID, exVector2{ 700,300 }, c2, exColor{ 212,175,200,255 }, 1);


	//check if ball out of bounds then reset
	if (myBall->p1.x < -10)
	{
		myBall->p1.x = 400;
		myBall->p1.y = 300;
		int direction[] = { -1,1 };
		int index = rand() % 2;
		int currentDirection = direction[index];
		int velX = (rand() % 6+1)*currentDirection;
		index = rand() % 2;
		currentDirection = direction[index];
		int velY = (rand() % 6 + 1)*currentDirection;
		myBall->p2 = exVector2{ (float)velX,(float)velY };
		Player2Score++;
	}
	else if (myBall->p1.x > 850)
	{
		myBall->p1.x = 400;
		myBall->p1.y = 300;
		int direction[] = { -1,1 };
		int index = rand() % 2;
		int currentDirection = direction[index];
		int velX = (rand() % 6 + 1)*currentDirection;
		index = rand() % 2;
		currentDirection = direction[index];
		int velY = (rand() % 6 + 1)*currentDirection;
		myBall->p2 = exVector2{ (float)velX,(float)velY };
		Player1Score++;
	}
	
	
}

