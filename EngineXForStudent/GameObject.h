#pragma once
#include "Engine/Public/EngineInterface.h"
class GameObject
{
public:
	GameObject();
	~GameObject();
	virtual void Tick(float fDeltaT);
	virtual void Render(exEngineInterface* mEngine);
	virtual void Colission(exVector2 pointA,exVector2 pointB);
	exVector2 p1;
	exVector2 p2;
	exColor c;
	float r;
};

