#pragma once
#include "GameObject.h"
class Border :public GameObject
{
public:
	Border();
	~Border();
	Border(exVector2 p1, exVector2 p2, exColor c);
	void Render(exEngineInterface* mEngine) override;
	void Tick(float tDelta) override;		
};

